// @flow
import { Observable, Subject } from 'rxjs';

/**
 * @deprecated use new Subject()
 *
 * @param name
 * @returns {*}
 */
export function createAction<T>(name: string): Subject<T> {
  const action = new Subject();
  action.subscribe((action: T) => {
    if (process.env.NODE_ENV === 'development') {
      console.debug(name, action);
    }
  });
  return action;
}

/**
 * @deprecated use new Subject()
 *
 * @param actionNames
 * @returns {*}
 */
export function createActions<T>(
  ...actionNames: Array<string>
): { [string]: Subject<T> } {
  return actionNames.reduce((akk, name) => ({ ...akk, [name]: createAction(name) }), {});
}

export type Reducer<T> = (state: T) => T;

export function createStore<T: Object>(
  name: string,
  reducer$: Observable<Reducer<T>>,
  initialState?: T,
  keepAlive: boolean = false
): Observable<T> {
  initialState = typeof initialState !== 'undefined' ? initialState : ({}: any);
  const store = reducer$
    .scan((state: T, reducer: Reducer<T>) => reducer(state), initialState)
    .startWith(initialState)
    .do((state: T) => {
      if (process.env.NODE_ENV === 'development') {
        window.console.log(name, state);
      }
    })
    .publishReplay(1)
    .refCount();
  if (keepAlive) {
    store.subscribe(() => {});
  }
  return store;
}

export default createStore;
